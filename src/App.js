import React from 'react';
import './App.css';
import Field from "./components/Field";

function App() {
  return (
    <div className="App">
      <Field initialValue='Teste'/>
    </div>
  );
}

export default App;
