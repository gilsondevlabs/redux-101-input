import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { changeValue } from "../fieldActions";

class Field extends Component {
    render() {
        return (
            <div>
                <label>{this.props.value}</label>
                <input type="text" onChange={this.props.changeValue} value={this.props.value} />
            </div>
        );
    }
}

// Ligar o estado (state) aos props do Componente
function mapStateToProps(state) {
    return {
        value: state.field.value
    };
}

// Ligar changeValue ao onChange do Componente
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changeValue
    }, dispatch)
}

// Efetuando a conexão do react com redux com connect
//
// state <==> props
// action <==> props
//
export default connect(mapStateToProps, mapDispatchToProps)(Field)