const INITIAL_STATE = { value: 'Opa' }

export default (state = INITIAL_STATE, action) => {
    console.log('Iniciando reducer')
    switch (action.type) {
        case 'VALUE_CHANGED':
            console.log('Action type: ' + action.type)
            return { value: action.payload }
        default:
            console.log('Action type: default')
            return state
    }
}