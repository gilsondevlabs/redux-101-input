// Ação criado para enviar o valor alterado
// para o reducer
export const changeValue = event => {
    console.log('Chamando action changeValue');
    return {
        type: 'VALUE_CHANGED',
        payload: event.target.value
    }
};